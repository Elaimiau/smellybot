import random
import re

stinky = [
    "Uh oh, {} is stinkyy",
    "Seems like {} diaper is yucky",
    "It looks like little {} is a messy baby",
    "{} is mushy wushy <:JoyfulUwU:785648485084299295>",
    "{} is the CEO of STINK",
]

clean = [
    "Good job {}, you are clean",
    "Seems like {} is a big kid who managed to stay dry, for now...",
    "Mathematically Impossible... {} is..... CLEAN? :pikachushock:",
]

soggy = [
    "It looks like {} was attacked by the sog monster",
    "{} is a soggy baby",
    "Aww, {} didn't manage to stay dry, seems like you are staying in diapers kiddo",
]

def handle_command(message):
    stinky_range = range(0,20) # %20
    soggy_range = range(20,60) # %40
    clean_range = range(60,100) # %40

    choice = random.randrange(0,100)

    text = ""

    if choice in stinky_range:
        text = random.choice(stinky)
    elif choice in soggy_range:
        text = random.choice(soggy)
    else:
        text = random.choice(clean)
    
    text = replace_emojis(message.channel.guild.emojis, text)
    
    victim = message.content.split(" ")[1]
    formatted_text = text.format(victim)

    return message.channel.send(formatted_text)

def replace_emojis(emojis, text):
    matcher = re.compile(r'\:\w+\:')
    to_replace = matcher.findall(text)

    if not to_replace:
        # there where no emojis to replace
        return text
    
    replaced_text = text

    for emoji in emojis:
        if f":{emoji.name}:" in to_replace:
            replaced_text = replaced_text.replace(
                f":{emoji.name}:",
                f"<:{emoji.name}:{emoji.id}>"
            )
    
    return replaced_text