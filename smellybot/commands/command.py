import enum
from typing import Optional, List

BOT_PREFIX = ">>"

class Command(enum.Enum):
    # the str part should be what is expected to be parsed from a discord message
    CHECK = "check"
    PSPSPS = "pspsps"
    UNKNOWN = "unknown"
    ROLE = "role"
    ROLL = "roll"
    FA = "fa"
    CHOOSE = "choose"

def parse_command(text: str) -> Command:
    tokens = text.split(" ")
    
    # Bot received an empty command
    if len(tokens) == 0:
        return Command.UNKNOWN
    
    received_command = get_command(tokens[0])

    return received_command

def get_command(cmd: str) -> Command:
    """Gets the command that corresponds with the given text"""
    try:
        return Command(cmd)
    except ValueError:
        return Command.UNKNOWN

def parse_tokens(message):
    return remove_prefix(message.content, BOT_PREFIX).split(" ")

def remove_prefix(text: str, prefix: str) -> str:
    if text.startswith(prefix):
        return text[len(prefix):]
    return text
