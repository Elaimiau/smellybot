import enum 
from typing import List

from discord.colour import Colour
from discord.permissions import Permissions
from ..command import parse_tokens

#TODO returning RoleCmd.UNKNOWN everywhere seems kinda messy and limiting
# a more flexible error handling would be beter (e.g. Error class that contains an error message, or store error
# message on UNKOWN??? dont know if possible)

class RoleCmd(enum.Enum):
    GET = "get"
    REMOVE = "remove"
    UNKNOWN = "unknown"

# >>role get ROLE [120,222,333 or COLORNAME]
# >>role remove ROLE
async def handle_command(message):
    tokens = parse_tokens(message)
    tokens = tokens[1:]
    
    # impl
    cmd = parse_role_cmd(tokens)

    if cmd == RoleCmd.UNKNOWN:
        await not_valid_cmd(tokens, message)
    if cmd == RoleCmd.GET:
        await get_role(tokens, message)
    if cmd == RoleCmd.REMOVE:
        await remove_role(tokens, message)

async def get_top_role_position(message):
    roles = await message.guild.fetch_roles()
    top = 0
    for r in roles:
        if r.position > top:
            top = r.position
    return top


async def role_exists(role: str, message):
    roles = await message.guild.fetch_roles()
    for r in roles:
        if r.name == role:
            return r
    return None

async def get_role(tokens, message):
    name = tokens[1]
    edit_role = False

    role = await role_exists(name, message)
    if role:
        edit_role = True
        #await message.channel.send(f"Role {name} already exists")

    rgb = to_rgb_values(tokens[2])
    r = rgb[0]
    g = rgb[1]
    b = rgb[2]

    color = Colour.from_rgb(r, g, b)
    print("user")
    for rl in message.author.roles:
        print(rl.name)
        print(rl.position)

    if role:
        print("top")
        top = await get_top_role_position(message)
        print(top)
        await role.edit(color=color, permissions=Permissions.none())
        await message.author.add_roles(role)
    else:
        top = await get_top_role_position(message)
        print("top")
        print(top)
        role = await message.guild.create_role(name=name, colour=color, permissions=Permissions.none())
        await role.edit(position=top+1, permissions=Permissions.none())
        print(role.position)
        await message.author.add_roles(role)

    await message.channel.send(f"Added role {name} to {message.author.display_name}")

def to_rgb_values(color: str) -> List[int]:
    # assume all rgb values in color are valid
    values_as_str = color.split(",")
    rgb_values = []
    for value in values_as_str:
        rgb_values.append(int(value))
    return rgb_values

def is_valid_get_role_cmd(tokens) -> bool:
    print("valid get")
    print(tokens)
    print(len(tokens))
    if len(tokens) != 3:
        return False
    print("number")

    # color should be:
    # rgb string e.g. 10,30,54
    # name of a valid discord color (dont know how to validate it besides hardcoding it) TODO validate this
    color = tokens[2]
    return is_valid_rgb(color)

def is_valid_rgb(raw_color_str: str) -> bool:
    tuple_str = raw_color_str.split(",")
    if len(tuple_str) != 3:
        return False
    
    for value in tuple_str:
        print("rgb")
        try:
            as_number = int(value)
            # valid rbg values are between 0 and 255
            if as_number > 255 or as_number < 0:
                return False
        except ValueError:
            # one of the parts wasn't a number
            return False
    return True

async def remove_role(tokens, message):
    # role exists
    name = tokens[1]
    user = message.author

    role_to_remove = None
    for role in user.roles:
        if role.name == name:
            role_to_remove = role
            break

    if not role_to_remove:
        await message.channel.send(f"Role {name} doesn't exist")
    # remove from member
    await user.remove_roles(role)
    await message.channel.send(f"Removed role {name} from {user.display_name}")

def is_valid_remove_role_cmd(tokens) -> bool:
    # just checking if all parts are there
    return len(tokens) == 2

async def not_valid_cmd(tokens, message):
    await message.channel.send("That command was not valid")

def parse_role_cmd(tokens):
    try:
        cmd =  RoleCmd(tokens[0])
    except ValueError:
        return RoleCmd.UNKNOWN
    
    print("basic")
    
    if cmd == RoleCmd.REMOVE and not is_valid_remove_role_cmd(tokens):
        cmd = RoleCmd.UNKNOWN

    if cmd == RoleCmd.GET and not is_valid_get_role_cmd(tokens):
        cmd = RoleCmd.UNKNOWN

    return cmd