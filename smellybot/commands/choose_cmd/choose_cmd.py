import random
from ..command import parse_tokens

async def handle_command(message):
    tokens = parse_tokens(message)
    tokens = tokens[1:]

    if tokens:
        choice = random.choice(tokens)
        text = f"{choice}"
        await message.channel.send(text)
    else:
        text = f"You didn't give me anything to choose, dumb baby"
        await message.channel.send(text)

