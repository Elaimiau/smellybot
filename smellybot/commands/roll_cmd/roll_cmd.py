from typing import List, Optional

from ..command import parse_tokens
from .dice import Dice

async def handle_command(message):
    tokens = parse_tokens(message)
    tokens = tokens[1:]

    if not tokens:
        await cant_roll_that(message)
    
    dices = parse_dices(tokens[0])

    if not dices:
        await message.channel.send("No dices or the dices were wrong")
    
    text = roll_dices(dices)
    
    await message.channel.send(text)

def roll_dices(dices) -> str:
    total = 0
    text = ""
    for dice in dices:
        roll = dice.roll()
        text = text + f"{roll} + "
        total = total + roll
    
    # remove extra '+'
    text = text[:-3]
    text = f"{text} = {total}"

    return text

async def cant_roll_that(message):
    text = "Can't roll that"
    await message.channel.send(text)

# dice_text e.g. d20+2,d10,d20,d4+2
def parse_dices(dice_text: str) -> Optional[List[Dice]]:
    dices = []
    for dice_str in dice_text.split(','):
        dice = Dice.from_str(dice_str)
        
        # A dice string was wrong, throw an error
        if not dice:
            return None
        
        dices.append(dice)
    
    return dices