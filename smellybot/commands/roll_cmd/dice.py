from typing import Optional
from random import randrange
import re


class Dice:

    DICE_MATCHER = re.compile(r'^d(\d+)(\+(\d+)){0,1}$')

    def __init__(self, number, modifier=0):
        self.number = number
        self.modifier = modifier
    
    def roll(self) -> int:
        return randrange(1, self.number+1) + self.modifier
    
    @classmethod
    def from_str(cls, dice_str):
        search = cls.DICE_MATCHER.search(dice_str)

        if not search:
            return None
        
        as_tuple = search.groups()

        if not as_tuple[2]:
            # no modifier
            number = int(as_tuple[0])
            return Dice(number)
        else:
            # there is modifier
            number = int(as_tuple[0])
            modifier = int(as_tuple[2])
            return Dice(number, modifier=modifier)