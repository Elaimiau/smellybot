from typing import Optional

class Entry:

    def __init__(self, links):
        self.links = links
        self.index = 0
    
    def next(self) -> Optional[str]:
        next_index = self.index + 1
        if len(self.links) == next_index:
            return None
        else:
            self.index = next_index
            return self.links[self.index]