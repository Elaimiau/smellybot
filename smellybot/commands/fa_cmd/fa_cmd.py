from ..command import parse_tokens
from .cache import Entry

from typing import Optional, List

import aiohttp
from bs4 import BeautifulSoup

FA = "https://www.furaffinity.net/"
FA_SEARCH = "search/?q="

async def handle_command(message, cache):
    search_words = parse_search_words(message)
    if not search_words:
        await no_search_words_were_passed(message)
        return
    
    if search_words[0] == "next":
        link = cache[message.author.name].next()
        if link:
            await message.channel.send(f"{FA}{link}")
        else:
            await message.channel.send("No more entries :(")
        return

    raw_body = await get_http(search_words)

    soup = BeautifulSoup(raw_body, 'html.parser')
    figures = soup.find_all('figure')

    if not figures:
        await no_matches_found_for_search(message)
        return
    
    links = extract_links(figures)
    cache[message.author.name] = Entry(links)

    await message.channel.send(f"{FA}{links[0]}")

def extract_links(figures):
    links = []
    for figure in figures:
        links.append(figure.a['href'])
    return links

async def no_search_words_were_passed(message):
    text = "No search parameters were given :("
    await message.channel.send(text)

async def no_matches_found_for_search(message):
    text = "Nothing of value was found"
    await message.channel.send(text)


async def get_http(search_words):
    search_query = ""
    for word in search_words:
        search_query += f"+{word}"

    search_query = search_query[1:] # remove extra + at start
    print(search_query)
    url = f"{FA}{FA_SEARCH}{search_query}"
    print(url)
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            # TODO add error handling for bad request
            return await response.text()

def parse_search_words(message) -> Optional[List[str]]:
    tokens = parse_tokens(message)
    tokens = tokens[1:] # remove "fa"

    if not tokens:
        # was empty, no search params were passed
        return None
    
    return tokens