import discord
import commands

BOT_PREFIX = ">>"

class SmellyClient(discord.Client):

    fa_cache = {}

    async def on_ready(self):
        print(f"Logged on as {self.user}")
    
    async def on_message(self, message):
        print(f"Message from {message.author}: {message.content}")

        if not is_for_bot(message):
            return
        
        parsed_cmd = commands.parse_command(commands.remove_prefix(message.content, BOT_PREFIX))

        if parsed_cmd == commands.Command.UNKNOWN:
            from commands.unknown_cmd import handle_command
            await handle_command(message)

        if parsed_cmd == commands.Command.CHECK:
            from commands.check_cmd import handle_command
            await handle_command(message)

        if parsed_cmd == commands.Command.CHOOSE:
            from commands.choose_cmd import handle_command
            await handle_command(message)

        if parsed_cmd == commands.Command.ROLL:
            from commands.roll_cmd import handle_command
            await handle_command(message)

        if parsed_cmd == commands.Command.FA:
            from commands.fa_cmd import handle_command
            await handle_command(message, self.fa_cache)

        if parsed_cmd == commands.Command.PSPSPS:
            await message.channel.send("Here kitty, kitty, pspspsps")

        if parsed_cmd == commands.Command.ROLE:
            # role command shutted down for now
            # it uses the unknown command
            #from commands.role_cmd import handle_command
            from commands.unknown_cmd import handle_command
            await handle_command(message)

def is_for_bot(message: str) -> bool:
    return message.content.startswith(BOT_PREFIX)