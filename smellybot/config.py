import os

class BotConfig():

    def __init__(self, token=None):
        if not token:
            self.token = os.environ["SMELLY_TOKEN"]
        else:
            self.token = token