from client import SmellyClient
from config import BotConfig

if __name__ == "__main__":
    config = BotConfig()
    client = SmellyClient()
    print("Starting")
    client.run(config.token)